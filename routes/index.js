var express = require('express');
var router = express.Router();
var assert = require('assert');
var mongoose = require('mongoose');
mongoose.connect('localhost:27017/test')
//mongoose.connect('mongodb://admin:admin@ds137090.mlab.com:37090/mymeandb')
var Schema=mongoose.Schema;
var session;
var msg;

var userDataSchema=new Schema({
    name:{type:String,require:true},
    email:{type:String,require:true,unique: true},
    contact:Number,
    passwd:{type:String,require:true}
},{collection:'userdata'});

var User=mongoose.model('userdata',userDataSchema);

var postDataSchema=new Schema({
    title:{type:String,require:true},
    data:{type:String,require:true},
    createdAt:{type:Date,default:Date.now},
    author:String,
    published:{type:Boolean,default:false}
},{collection:'postdata'});

var Post=mongoose.model('postdata',postDataSchema);

router.post('/insert',function (req,res,next){
  var user={
    name:req.body.name,
    email:req.body.email,
    contact:req.body.contact,
    passwd:req.body.passwd
  };
  var data=new User(user);
  data.save();
  res.render('signin',{title: 'Log In',msg:'Sign Up successfull please Sign in to continue...' ,layout: 'layout.hbs'});
});

router.post('/create-post',function (req,res,next){
  var post={
    title:req.body.title,
    data:req.body.data,
    author:req.body.author
    };
    var data=new Post(post);
    if(data.save()){
      console.error('Created');
    }
    res.redirect('/posts');
});

router.post('/fn_login',function(req,res){
    var email=req.body.email;
    var passwd=req.body.passwd;
    User.findOne({email:email,passwd:passwd},function(err,user){
      if(err){
        console.log(err);
      }
      if(!user){
        console.log("No User Found");
        msg = 'Incorrect username or password';
        res.redirect('/signin');
      }
      else{
        req.session.user=user;
        console.log(req.session.user);
        res.redirect('/dashboard');
        console.log("go");
      }
    })
})

router.get('/', function(req, res, next) {
  Post.find()
  .then(function(doc){
  res.render('index',{ title: 'Home' ,layout: 'layout.hbs',post:doc});
});
});

router.get('/posts', function(req, res, next) {
  if(!req.session.user)
  {
    res.render('signin',{title: 'Log In',msg:'Please Sign in to continue...' ,layout: 'layout.hbs'});
  }
  else
  {
    Post.find()
    .then(function(doc){
    res.render('system/posts',{title: 'Posts',session:req.session.user,post:doc,layout: 'dash.hbs'});
  })
}
});

router.get('/users', function(req, res, next) {
  if(!req.session.user)
  {
    res.render('signin',{title: 'Log In',session:req.session.user,msg:'Please Sign in to continue...' ,layout: 'layout.hbs'});
  }
  else{
      User.find()
      .then(function(doc){
      res.render('system/users',{title: 'Users',user:doc,session:req.session.user,layout: 'dash.hbs'});
    });
  }
});

router.get('/signin', function(req, res, next) {
  res.render('signin',{ title: 'Sign In',msg: msg ,layout: 'layout.hbs'});
});


router.get('/updateuser/:id', function(req, res, next) {
  var id=req.params.id;
  User.findById(id, function(err,doc){
    res.render('system/update-user',{title: 'Users',user:doc,id:id ,layout: 'dash.hbs'});
  });
});

router.get('/signup', function(req, res, next) {
  res.render('signup',{title: 'Sign Up',layout: 'layout.hbs'});
});

router.post('/update',function (req,res,next){
  var id=req.body.id;
  User.findById(id, function(err,doc){
    if(err){
      console.error('No User Found')
    }
    doc.name=req.body.name;
    doc.email=req.body.email;
    doc.contact=req.body.contact;
    doc.save();
    res.redirect('/users');
  });
});

router.get('/delete/:id',function (req,res,next){
  var id=req.params.id;
  User.findByIdAndRemove(id).exec();
  res.redirect('/users');
});

router.get('/delete-post/:id',function (req,res,next){
  var id=req.params.id;
  Post.findByIdAndRemove(id).exec();
  res.redirect('/posts');
});

router.get('/logout',function (req,res,next){
  req.session.user="";
  console.log(req.session.user);
  res.render('signin',{title: 'Log In',msg:'Logged Out',layout: 'layout.hbs'});
});

router.get('/dashboard',function (req,res,next){
  if(!req.session.user)
  {
    res.render('signin',{title: 'Log In',msg:'Please Sign in to continue...' ,layout: 'layout.hbs'});
  }
  else{
    res.render('system/dashboard',{ title: 'Dashboard',session:req.session.user ,layout: 'dash.hbs'});
    console.error('welcomeeee')
  }
});

module.exports = router;
